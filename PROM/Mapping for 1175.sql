-- Mapping for skjema 1175, ROAS utsendelse i 2021

select * from PROM.FormMapping where FormId = 1175;
select * from PROM.FieldMapping where PromId = 60;
GO

EXEC PROM.AddFieldMapping 60, 'VAR3007', 3007, 'Formulering burde kanskje v�rt "i 2020"?';
EXEC PROM.AddFieldMapping 60, 'enum30', 3008, 'Mapping detaljert gjennomg�tt';

-- Medisinbruk for Addisons sykdom

EXEC PROM.AddFieldMapping 60, 'enum34', 11481, 'En eller flere typer';
EXEC PROM.AddFieldMapping 60, 'VAR1807', 1807;
EXEC PROM.AddFieldMapping 60, 'VAR1808', 1808;
EXEC PROM.AddFieldMapping 60, 'enum35', 11482;
EXEC PROM.AddFieldMapping 60, 'number22', 11487;
EXEC PROM.AddFieldMapping 60, 'VAR1809', 1809;

-- Florinef

EXEC PROM.AddFieldMapping 60, 'VAR1810', 1810;

-- Pasientinformasjon og komplikasjoner

EXEC PROM.AddFieldMapping 60, 'VAR1811', 1811;
EXEC PROM.AddFieldMapping 60, 'VAR1812', 1812;
EXEC PROM.AddFieldMapping 60, 'enum13', 1868;
EXEC PROM.AddFieldMapping 60, 'VAR1813', 1813;

-- Binyrekriser

EXEC PROM.AddFieldMapping 60, 'VAR10574', 10574, 'Binyrekrise 2020';

EXEC PROM.AddFieldMapping 60, 'VAR10579', 10579, 'Januar';
EXEC PROM.AddFieldMapping 60, 'VAR10580', 10580, 'Februar';
EXEC PROM.AddFieldMapping 60, 'VAR10581', 10581, 'Mars';
EXEC PROM.AddFieldMapping 60, 'VAR10582', 10582, 'April';
EXEC PROM.AddFieldMapping 60, 'VAR10583', 10583, 'Mai';
EXEC PROM.AddFieldMapping 60, 'VAR10584', 10584, 'Juni';
EXEC PROM.AddFieldMapping 60, 'VAR10585', 10585, 'Juli';
EXEC PROM.AddFieldMapping 60, 'VAR10586', 10586, 'August';
EXEC PROM.AddFieldMapping 60, 'VAR10587', 10587, 'September';
EXEC PROM.AddFieldMapping 60, 'VAR10588', 10588, 'Oktober';
EXEC PROM.AddFieldMapping 60, 'VAR10589', 10589, 'November';
EXEC PROM.AddFieldMapping 60, 'VAR10590', 10590, 'Desember';

EXEC PROM.AddFieldMapping 60, 'VAR1815', 1815, 'Kjernejournal';

-- Andre diagnoser

EXEC PROM.AddFieldMapping 60, 'enum41', 11498, 'Hypertyreose';
EXEC PROM.AddFieldMapping 60, 'number28', 11499, 'Diagnose�r';
EXEC PROM.AddFieldMapping 60, 'enum42', 11500, 'Autoimmun hypotyreose';
EXEC PROM.AddFieldMapping 60, 'number29', 11501, 'Diagnose�r';
EXEC PROM.AddFieldMapping 60, 'enum43', 11502, 'Type 1 diabetes';
EXEC PROM.AddFieldMapping 60, 'number30', 11503, 'Diagnose�r'; -- Vurdere � bytte mapping her?
EXEC PROM.AddFieldMapping 60, 'enum44', 11504, 'C�liaki';
EXEC PROM.AddFieldMapping 60, 'number31', 11505, 'Diagnose�r'; 

-- Medisiner mot andre tilstander

EXEC PROM.AddFieldMapping 60, 'VAR1825', 1825, 'Levaxin'; 
EXEC PROM.AddFieldMapping 60, 'VAR1826', 1826, 'Blodrykksmedisin'; 
EXEC PROM.AddFieldMapping 60, 'VAR1887', 1887, 'Blodrykksmedisin spesifikasjon'; 
EXEC PROM.AddFieldMapping 60, 'VAR1827', 1827, 'Kolesterolmedisin'; 
EXEC PROM.AddFieldMapping 60, 'VAR1888', 1888, 'Hvilken medisin'; 
EXEC PROM.AddFieldMapping 60, 'VAR1828', 1828, 'Hjerteinfarkt'; 

-- Infeksjoner

EXEC PROM.AddFieldMapping 60, 'enum36', 11506, 'Influensavaksine'; 
EXEC PROM.AddFieldMapping 60, 'VAR1829', 1829, 'Antibiotika osv.'; 
EXEC PROM.AddFieldMapping 60, 'VAR1894', 1894, 'Bakterieinfeksjon';  -- boolean
EXEC PROM.AddFieldMapping 60, 'VAR1895', 1895, 'Soppinfeksjon';      -- boolean
EXEC PROM.AddFieldMapping 60, 'VAR1896', 1896, 'Virusinfeksjon';     -- boolean
EXEC PROM.AddFieldMapping 60, 'VAR1897', 1897, 'Antall antibiotikakurer siste �ret'; 

-- Andre sykdommer / medisiner

EXEC PROM.AddFieldMapping 60, 'VAR1830', 1830, 'Nye sykdommer siste �ret'; 
EXEC PROM.AddFieldMapping 60, 'VAR1893', 1893, 'Hvilke diagnoser'; 
EXEC PROM.AddFieldMapping 60, 'VAR1831', 1831, 'Andre medisiner enn oppgitt'; 
EXEC PROM.AddFieldMapping 60, 'VAR1889', 1889, 'Hvilke medisiner'; 

-- Arbeidsforhold

EXEC PROM.AddFieldMapping 60, 'VAR1832', 1832, 'Arbeid'; 
EXEC PROM.AddFieldMapping 60, 'VAR1892', 1892, 'Stillingsprosent'; 
EXEC PROM.AddFieldMapping 60, 'VAR1891', 1891, 'Sykmeldingsdager'; 
EXEC PROM.AddFieldMapping 60, 'VAR1833', 1833, 'Uf�retrygd'; 
EXEC PROM.AddFieldMapping 60, 'VAR1890', 1890, 'Prosentandel'; 

-- Besvares kun av kvinner

EXEC PROM.AddFieldMapping 60, 'VAR9069', 9069, 'Overgangsalder'; 
EXEC PROM.AddFieldMapping 60, 'VAR6334', 6334, 'Alder ved siste mens'; 